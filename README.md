# anonscm rewrite map generator

This repository is the source of the anonscm.debian.org to salsa.debian.org
rewrite map. This rewrite map will be supported after disabling alioth.

## Definition files

A definition file is a simple file that is organized in two columns. First
column contains the original repo beginning with the alioth project (without
the trailing ".git") and the second column contains the new salsa group and
the project name. You can also add comments with lines starting with #.

Example:
```
   # this is a comment 
   collab-maint/dealer debian/dealer
``` 

Definitions are organized in files with filenames ending on .conf in the definitions
folder. 

## contribute

If you want to get your migrated project redirected to salsa send a pull-request
against the [AliothRewriter Project](https://salsa.debian.org/salsa/AliothRewriter). 
If you have only one or two rewrites append them to the *general.conf* file. If you have
more redirects just create a new file ending with *.conf*. 
